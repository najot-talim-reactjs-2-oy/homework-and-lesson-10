import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input } from 'antd';
import smoke from '../../assets/smoke3.mp4'

import './login.css';
import VanillaTilt from 'vanilla-tilt';
import { useEffect, useRef } from 'react';

import { useNavigate } from "react-router-dom";

const LoginP = () => {
 
  const tiltRef = useRef(null);
  useEffect(() => {
    if (tiltRef.current) {
      VanillaTilt.init(tiltRef.current, {
        max: 25, // Maximum tilt rotation (degrees)
        speed: 400, // Speed of the tilt transition (milliseconds)
        glare: true, // Add glare effect (boolean)
        'max-glare': 0.5 // Maximum glare opacity (number between 0 and 1)
      });
    }
    return () => {
      // Clean up the Vanilla Tilt instance when the component is unmounted
      if (tiltRef.current) {
        tiltRef.current.vanillaTilt.destroy();
      }
    };
  }, []);
  const navigate = useNavigate();
  const submit = () =>{
    navigate("/dashboard")
  }
  
  return (
    <div>
      <div className='glass'>
        {/* ref={tiltRef} className='tilted-card' */}
        <div ref={tiltRef} className='tilted-card'>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
            
          >
              <h2>Welcome</h2>
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please input your Username!',
                },
              ]}
            >
              <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input your Password!',
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
              />
            </Form.Item>


            <Form.Item>
              <Button type="primary" htmlType="submit" id='bt' className="login-form-button" value="Send" onClick={submit}>
                Log in
              </Button>
              <span style={{color: 'white'}}>Or</span> <a style={{color: "white", textDecoration: "underline"}} href="">register now!</a>
            </Form.Item>
          </Form>
        </div>
      </div>
      <video autoPlay loop>
        <source src={smoke}/>
      </video>
    </div>
  )
}

export default LoginP