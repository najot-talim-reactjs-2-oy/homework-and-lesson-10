import { DollarOutlined, HomeOutlined, UserOutlined, UsergroupAddOutlined } from "@ant-design/icons"
import './dashboard.css'
import SimpleChart from "./SimpleChart"
import { Pie } from "@ant-design/plots";
import users from './users.json'

const DashboardP = () => {
  const homme = users.filter(u=>u.genre === "M")
  const femme = users.filter(u=>u.genre === "F")
  const name = users.filter(u=> u.genre === "N")
  const io = users.filter(u => u.genre === "I")
  const data = [
    {
      type: "Homme",
      value: homme.length,
    },
    {
      type: "Femme",
      value: femme.length,
    },
    {
      type: "Namme",
      value: name.length,
    },
    {
      type: "Ioa",
      value: io.length,
    },
  ];
  const config = {
    appendPadding: 10,
    data,
    angleField: "value",
    colorField: "type",
    radius: 0.75,
    label: {
      type: "spider",
      labelHeight: 28,
      content: "{name}\n{percentage}",
    },
    interactions: [
      {
        type: "element-selected",
      },
      {
        type: "element-active",
      },
    ],
  };
  return (
    <div>
      <div className="flex_dashboard_header">
        <div className="student">
          <div className="text_st_dashboard">
            <h2>2192 <span>Students</span></h2>
          </div>
          <h1 style={{fontSize: "35px"}}><UsergroupAddOutlined /></h1>
        </div>
        <div className="student">
          <div className="text_st_dashboard">
            <h2>53 <span>Teachers</span></h2>
          </div>
          <h1 style={{fontSize: "35px"}}><UserOutlined /></h1>
        </div>
        <div className="student">
          <div className="text_st_dashboard">
            <h2>5 <span>Schools</span></h2>
          </div>
          <h1 style={{fontSize: "35px"}}><HomeOutlined /></h1>
        </div>
        <div className="student">
          <div className="text_st_dashboard">
            <h2>3500 <span>Students</span></h2>
          </div>
          <h1 style={{fontSize: "35px"}}><DollarOutlined /></h1>
        </div>
      </div>
      <br />
      <div>
        <SimpleChart />
      </div>
      <div>
        <Pie {...config} />
      </div>
    </div>
  )
}

export default DashboardP