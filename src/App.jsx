import { BrowserRouter, Route, Routes } from "react-router-dom"
import LayoutAdmin from "./components/layout/Layout"
import DashboardP from "./pages/dashboard/DashboardP"
import StudentsP from "./pages/students/StudentsP"
import TeachersP from "./pages/teachers/TeachersP"

import NotFoundP from "./pages/notfound/NotFoundP"
import LoginP from "./pages/login/LoginP"



function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LoginP />} />
        <Route path="/" element={<LayoutAdmin />}>
          <Route path="dashboard" element={<DashboardP />} />
          <Route path="students" element={<StudentsP />} />
          <Route path="teachers" element={<TeachersP />} />
        </Route>
        <Route path="*" element={<NotFoundP />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
